"""Test the "binpackers" module"""
from typing import List
import uuid
import pytest
from datastreamcorelib.binpackers import (
    ensure_utf8,
    ensure_utf8_list,
    ensure_str,
    ensure_str_list,
    msgpack_unpack,
    msgpack_pack,
    normalize_uri_topic_list,
    uuid_to_b64,
    b64_to_uuid,
)


@pytest.mark.parametrize("test_input", ["asciistring", "Hääyöaie"])
def test_str2bytes(test_input: str) -> None:
    """Make sure input string gets converted to utf8 bytes"""
    expected = test_input.encode("utf-8")
    assert ensure_utf8(test_input) == expected


def test_bytes2bytes() -> None:
    """Make sure bytes already encoded are not converted"""
    encoded = "Hääyöaie".encode("utf-8")
    assert ensure_utf8(encoded) == encoded


def test_str2str() -> None:
    """Make sure strings are not converted again"""
    decoded = "Hääyöaie"
    assert ensure_str(decoded) == decoded


@pytest.mark.parametrize("test_input", [b"asciistring", b"H\xc3\xa4\xc3\xa4y\xc3\xb6aie"])
def test_bytes2str(test_input: bytes) -> None:
    """Make sure bytes are decoded to strings"""
    expected = test_input.decode("utf-8")
    assert ensure_str(test_input) == expected


@pytest.mark.parametrize("test_input", [["asciistring", "Hääyöaie"]])
def test_liststr2listbytes(test_input: List[str]) -> None:
    """Make sure single input string gets converted to with one element of utf8 bytes"""
    expected = [itm.encode("utf-8") for itm in test_input]
    assert ensure_utf8_list(test_input) == expected
    # Check the bytes2bytes case too
    assert ensure_utf8_list(expected) == expected


@pytest.mark.parametrize("test_input", [[b"asciistring", b"H\xc3\xa4\xc3\xa4y\xc3\xb6aie"]])
def test_listbytes2liststr(test_input: List[bytes]) -> None:
    """Make sure single input string gets converted to with one element of utf8 bytes"""
    expected = [itm.decode("utf-8") for itm in test_input]
    assert ensure_str_list(test_input) == expected
    # check the str2str case too
    assert ensure_str_list(expected) == expected


def test_unpack() -> None:
    """Test msgpack unpacking with trivial dict"""
    decoded = msgpack_unpack(b"\x81\xa6strkey\xa6strval")
    assert "strkey" in decoded
    assert decoded["strkey"] == "strval"


def test_pack() -> None:
    """Test msgpack packing with trivial dict"""
    assert msgpack_pack({"strkey": "strval"}) == b"\x81\xa6strkey\xa6strval"


def test_pack_unpack_roundtrip() -> None:
    """Check that what we pack unpacks to the same"""
    test_dict = {"strkey": b"strkeybinval", b"binkey": "binkeystrval", "strkey2": ["listval1str", b"listval2bin"]}
    encoded = msgpack_pack(test_dict)
    decoded = msgpack_unpack(encoded)
    assert decoded == test_dict


def test_topic_normalize() -> None:
    """Check that the topic/socket normalization func works"""
    expected = [b"This is my value"]
    assert normalize_uri_topic_list([b"This is my value"]) == expected
    assert normalize_uri_topic_list(b"This is my value") == expected
    assert normalize_uri_topic_list("This is my value") == expected


def test_uuid_encode_decode_roundtrip() -> None:
    """Test UUID encode and decode"""
    expected = uuid.uuid4()
    encoded_str = uuid_to_b64(expected)
    encoded_bytes = encoded_str.encode("ascii")
    decoded_str = b64_to_uuid(encoded_str)
    decoded_bytes = b64_to_uuid(encoded_bytes)
    assert decoded_str == expected
    assert decoded_bytes == expected
