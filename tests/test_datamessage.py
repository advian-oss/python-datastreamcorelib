"""Test datamessage packing / unpacking."""
from typing import cast
import pytest

from datastreamcorelib.datamessage import PubSubDataMessage
from datastreamcorelib.pubsub import Subscription
from datastreamcorelib.abstract import ZMQSocketType, ZMQSocketDescription, TooFewPartsError

from .fixtures import FakeSocketHandler, TrackingSubHandler, FakePubSubManager


def test_decode() -> None:
    """Test decoding multipart message."""
    dmsg = PubSubDataMessage.zmq_decode(
        [
            b"datatopic",
            b"\r\xcf\x11\x8a\xb4OF\x8b\xa7\x99(\xc7c\x12\x17\x05",
            b"\x81\xa6strkey\xa6strval",
        ]
    )
    # Make sure we do not auto-add systemtime to messages that did not have it
    assert "systemtime" not in dmsg.data
    assert dmsg.topic == b"datatopic"
    assert str(dmsg.messageid) == "0dcf118a-b44f-468b-a799-28c763121705"
    assert dmsg.data["strkey"] == "strval"


def test_encode_decode_roundtrip() -> None:
    """Make sure we can decode what we encode."""
    dmsg = PubSubDataMessage("enctopic")
    # Make sure systemtime was auto-added on normal init
    assert "systemtime" in dmsg.data
    # Make sure marked to be in UTC.
    assert dmsg.data["systemtime"].endswith("Z")
    dmsg.data["strkey"] = "strval"
    dmsg.data[b"binkey"] = b"binval"
    dmsg2 = PubSubDataMessage.zmq_decode(dmsg.zmq_encode())
    assert dmsg2.topic == dmsg.topic
    assert dmsg2.messageid == dmsg.messageid
    assert dmsg2.data == dmsg.data


def test_sm_dispatch_decode() -> None:
    """Test subscription callback dispatching and datamessage decoding"""
    cbhdl = TrackingSubHandler()
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr1", b"addr2"], ZMQSocketType.SUB)
    dmsg = PubSubDataMessage("sm_rountrid_test")
    dmsg.data["strkey"] = "strval"
    dmsg.data[b"binkey"] = b"binval"

    subdesc = Subscription(sockdesc.socketuris, dmsg.topic, cbhdl.success_callback, decoder_class=PubSubDataMessage)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)
    sminstance.subscribe(subdesc)

    # pass a raw message to the manager
    sminstance.raw_zmq_callback(sockinst, dmsg.zmq_encode())

    # Check that we got something sane
    subkey = subdesc.trackingid
    assert subkey in cbhdl.messages_by_sub
    assert cbhdl.messages_by_sub[subkey]
    # We know the message is actually this subclass, see https://mypy.readthedocs.io/en/latest/casts.html
    rmsg = cast(PubSubDataMessage, cbhdl.messages_by_sub[subkey][0])
    assert rmsg.topic == dmsg.topic
    assert rmsg.data == dmsg.data


def test_decode_error_toofew() -> None:
    """Test decoding multipart message with too few parts"""
    with pytest.raises(TooFewPartsError):
        PubSubDataMessage.zmq_decode([b"datatopic", b"\r\xcf\x11\x8a\xb4OF\x8b\xa7\x99(\xc7c\x12\x17\x05"])


def test_decode_error_msgpack() -> None:
    """Test decoding with truncated msgpack binary"""
    with pytest.raises(ValueError):
        PubSubDataMessage.zmq_decode(
            [
                b"datatopic",
                b"\r\xcf\x11\x8a\xb4OF\x8b\xa7\x99(\xc7c\x12\x17\x05",
                b"\x81\xa6str\x00key\xa6strva",
            ]
        )


def test_decode_error_uuid() -> None:
    """Test decoding with truncated UUID binary"""
    with pytest.raises(ValueError):
        PubSubDataMessage.zmq_decode(
            [
                b"datatopic",
                b"\r\xcf\x11\x8a\xb4OF\x8b\xa7\x99(\xc7c\x12\x17",
                b"\x81\xa6str\x00key\xa6strval",
            ]
        )
