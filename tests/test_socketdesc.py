"""Test socket descriptions"""
from datastreamcorelib.abstract import ZMQSocketDescription, ZMQSocketType


def test_socket_equality() -> None:
    """Check that two socket descriptors initialized with same values are equal"""
    desc1 = ZMQSocketDescription("ipc://foo.bar")
    desc2 = ZMQSocketDescription("ipc://foo.bar")
    desc3 = ZMQSocketDescription("ipc://foo.bar", ZMQSocketType.PUB)
    assert desc1 == desc2
    assert desc1.__hash__() == desc2.__hash__()  # pylint: disable=C2801
    assert desc3 != desc1
    assert desc3.__hash__() != desc1.__hash__()  # pylint: disable=C2801
