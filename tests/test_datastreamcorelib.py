"""package level tests"""
from datastreamcorelib import __version__


def test_version() -> None:
    """Make sure the version string matches expected"""
    assert __version__ == "1.6.0"
