"""Test fixtures"""
from typing import List, Dict
from dataclasses import dataclass, field
import uuid

from datastreamcorelib.abstract import ZMQSocket, ZMQSocketType, BaseSocketHandler, ZMQSocketDescription
from datastreamcorelib.pubsub import Subscription, PubSubMessage, BasePubSubManager


@dataclass
class FakeSocket(ZMQSocket):
    """Fake socket implementation"""

    socket_type: ZMQSocketType
    topics: List[bytes] = field(default_factory=list, init=False, repr=True)
    connected: List[bytes] = field(default_factory=list, init=False, repr=True)
    bound: List[bytes] = field(default_factory=list, init=False, repr=True)
    _hash: int = field(init=False, repr=False)
    _closed: bool = field(init=False, repr=False, default=False)

    def __post_init__(self) -> None:
        """Set our hash"""
        self._hash = uuid.uuid4().int

    def subscribe(self, topic: bytes) -> None:
        """Subscribe to given topic"""
        self.topics.append(topic)

    def connect(self, addr: bytes) -> None:
        """Connect to given socket address"""
        self.connected.append(addr)

    def bind(self, addr: bytes) -> None:
        """Connect to given socket address"""
        self.bound.append(addr)

    @property
    def closed(self) -> bool:
        return self._closed

    def close(self) -> None:
        """Close the socket"""
        self._closed = True

    def __hash__(self) -> int:
        """Return our random uuid bytes as the "hash" """
        return self._hash


@dataclass
class FakeSocketHandler(BaseSocketHandler):
    """Implement socket fetching"""

    def get_socket(self, desc: ZMQSocketDescription) -> ZMQSocket:
        """Get socket by address, or bind new one."""
        sock = self._get_cached_socket(desc)
        if sock:
            if not sock.closed:
                return sock
            # Clear the cache and create a new socket.
            del self._sockets_by_desc[desc]

        sock = FakeSocket(desc.sockettype)
        # Bind/connect as needed
        if desc.sockettype not in (ZMQSocketType.SUB, ZMQSocketType.PUB, ZMQSocketType.REQ, ZMQSocketType.REP):
            raise RuntimeError("Don't know how to handle type {}".format(desc.sockettype))
        # We know the ZMQSocketDescription cast these into correct type
        if desc.sockettype in (ZMQSocketType.SUB, ZMQSocketType.REQ):
            for addr in desc.socketuris:
                sock.connect(addr)  # type: ignore
        if desc.sockettype in (ZMQSocketType.PUB, ZMQSocketType.REP):
            for addr in desc.socketuris:
                sock.bind(addr)  # type: ignore

        self._set_cached_socket(desc, sock)
        return sock


@dataclass
class TrackingSubHandler:
    """Simple callback handler that will put messages into lists organized by the subscription"""

    messages_by_sub: Dict[uuid.UUID, List[PubSubMessage]] = field(default_factory=dict)

    def success_callback(self, sub: Subscription, msg: PubSubMessage) -> None:
        """Append the message to the correct list"""
        if sub.trackingid not in self.messages_by_sub:
            self.messages_by_sub[sub.trackingid] = []
        self.messages_by_sub[sub.trackingid].append(msg)


class FakePubSubManager(BasePubSubManager):
    """Implementation of pubsubmanager"""

    def _publish_actual(self, msg: PubSubMessage, sock: ZMQSocket) -> None:
        """No-Op implementation for actually handling the send"""
        print("{} out of {}".format(msg, sock))


class CBTestException(RuntimeError):
    """Just for testing exception raising in callbacks"""
