"""Tests for the subscription class."""
import pytest

from datastreamcorelib.pubsub import PubSubMessage, Subscription, SUBSCRIPTIONMANAGER_SINGLETONS, NoDefaultSocketError
from datastreamcorelib.abstract import ZMQSocketType, ZMQSocketDescription


from .fixtures import FakeSocketHandler, TrackingSubHandler, FakePubSubManager


def simple_callback(sub: Subscription, msg: PubSubMessage) -> None:
    """Trivial callback."""
    print(sub, msg)


def test_sockets_topics_bytes() -> None:
    """Make sure the sockets and topics are converted to lists of bytes."""
    sub = Subscription(["ipc://foo", "bar"], "topic", simple_callback)
    assert sub.socketuris[1] == b"bar"
    assert sub.topics == (b"topic",)


def test_sm_singleton() -> None:
    """Test that we can generate singletons valid SubscriptionManager"""
    sminstance1 = FakePubSubManager.instance(FakeSocketHandler)
    assert isinstance(sminstance1.sockethandler, FakeSocketHandler)
    assert sminstance1.sockethandler == FakeSocketHandler.instance()
    old_len = len(SUBSCRIPTIONMANAGER_SINGLETONS)
    assert old_len
    sminstance2 = FakePubSubManager.instance(FakeSocketHandler)
    assert sminstance1 == sminstance2
    assert len(SUBSCRIPTIONMANAGER_SINGLETONS) == old_len


def test_sm_subscribe() -> None:
    """Test the subscription methods of SubscriptionManager"""
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    subdesc = Subscription(["ipc://foo", "bar"], "topic", simple_callback)
    sminstance.subscribe(subdesc)
    assert subdesc in sminstance.subscriptions


def test_sm_dispatch_prefix() -> None:
    """Test subscription callback dispatching"""
    cbhdl = TrackingSubHandler()
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr1", b"addr2"], ZMQSocketType.SUB)

    # Subscribe to a prefix of the real publish topic to test prefix matching
    subdesc = Subscription(sockdesc.socketuris, "dispatch_test", cbhdl.success_callback)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)
    sminstance.subscribe(subdesc)

    # pass a raw message to the manager
    sminstance.raw_zmq_callback(sockinst, [b"dispatch_test_1", b"baz", b"plonk"])

    # Check that we got something sane
    subkey = subdesc.trackingid
    assert subkey in cbhdl.messages_by_sub
    assert cbhdl.messages_by_sub[subkey]
    assert cbhdl.messages_by_sub[subkey][0].topic == b"dispatch_test_1"
    assert cbhdl.messages_by_sub[subkey][0].dataparts == [b"baz", b"plonk"]

    # pass another raw message to the manager
    sminstance.raw_zmq_callback(sockinst, [b"dispatch_test_2", b"baz", b"plonk"])
    assert subkey in cbhdl.messages_by_sub
    assert cbhdl.messages_by_sub[subkey]
    assert cbhdl.messages_by_sub[subkey][1].topic == b"dispatch_test_2"
    assert cbhdl.messages_by_sub[subkey][1].dataparts == [b"baz", b"plonk"]


def test_sm_publish_signatures() -> None:
    """Test the publish method with various signatures"""
    mgr = FakePubSubManager.instance(FakeSocketHandler)
    msg = PubSubMessage.zmq_decode([b"signaturetest", b"plim", b"plom"])
    sdesc = ZMQSocketDescription([b"signaturetest_addr1", b"signaturetest_addr2"], ZMQSocketType.PUB)

    # URI normalizations
    mgr.publish(msg, "ipc://signaturetest_addr_normalize")
    mgr.publish(msg, b"ipc://signaturetest_addr_normalize")
    mgr.publish(msg, ["ipc://signaturetest_addr_normalize"])
    mgr.publish(msg, [b"ipc://signaturetest_addr_normalize"])

    # Explicit socket
    sock = mgr.sockethandler.get_socket(sdesc)
    mgr.publish(msg, sock)

    # Make sure no default socket raises error
    mgr.default_pub_socket = None
    with pytest.raises(NoDefaultSocketError):
        mgr.publish(msg)

    # Set default socket and make sure it works
    mgr.default_pub_socket = sock
    mgr.publish(msg)
