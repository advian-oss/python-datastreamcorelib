"""Test imagemessage packing/unpacking"""
from dataclasses import dataclass, field
import logging
import re
import pytest

from datastreamcorelib.logging import init_logging
from datastreamcorelib.datamessage import PubSubDataMessage
from datastreamcorelib.imagemessage import PubSubImageMessage, ImgFormatError, MissingImgAttributeError
from datastreamcorelib.abstract import TooFewPartsError


@dataclass  # pylint: disable=R0901
class MyFormatImageMsg(PubSubImageMessage):
    """Special format handling"""

    sanity_check_called: bool = field(default=False)

    def sanity_check_myformat(self) -> None:
        """Dummy sanity-checker"""
        self.sanity_check_called = True


def red_pixel_imagemessage(topic: str = "imgtopic") -> PubSubImageMessage:
    """Make message with one red pixel"""
    msg = PubSubImageMessage(topic)
    # Make one red pixel
    msg.imginfo["format"] = "bgr8"
    msg.imginfo["w"] = 1
    msg.imginfo["h"] = 1
    msg.imgdata = bytes((0, 0, 255))
    # Make sure we're sane
    msg.sanity_check_imgdata()
    return msg


def test_enc_dec_roundtrip() -> None:
    """Make sure we can decode a message that is sane before encoding"""
    msg = red_pixel_imagemessage("imgtopic")
    assert msg.topic == b"imgtopic"
    assert "systemtime" in msg.data
    assert "imginfo" in msg.data
    assert "format" in msg.data["imginfo"]
    msg.data["foobar"] = "baz"

    parts = msg.zmq_encode()
    dmsg = PubSubImageMessage.zmq_decode(parts)
    assert dmsg.imgdata == msg.imgdata
    for key in msg.imginfo:
        assert msg.imginfo[key] == dmsg.imginfo[key]
        assert dmsg.data["imginfo"][key] == dmsg.imginfo[key]


def test_decode_as_dm() -> None:
    """Make sure that image messages can be decoded as datamessages"""
    msg = red_pixel_imagemessage()
    msg.data["foobar"] = "baz"
    parts = msg.zmq_encode()
    dmsg = PubSubDataMessage.zmq_decode(parts)
    for key in msg.imginfo:
        assert msg.imginfo[key] == dmsg.data["imginfo"][key]
    assert dmsg.data["foobar"] == "baz"


def test_too_few_parts() -> None:
    """Test too few message parts"""
    with pytest.raises(TooFewPartsError):
        msg = PubSubImageMessage.zmq_decode(
            [b"datatopic", b"\r\xcf\x11\x8a\xb4OF\x8b\xa7\x99(\xc7c\x12\x17\x05", b"\x81\xa6strkey\xa6strval"]
        )
        assert msg


def test_missing_imginfo() -> None:
    """Missing imgformat key"""
    with pytest.raises(MissingImgAttributeError):
        msg = PubSubImageMessage.zmq_decode(
            [b"datatopic", b"\r\xcf\x11\x8a\xb4OF\x8b\xa7\x99(\xc7c\x12\x17\x05", b"\x81\xa6strkey\xa6strval", b""]
        )
        assert msg


def test_wrong_imgdata_len() -> None:
    """Test that decoding fails if we have size mismatch"""
    msg = red_pixel_imagemessage()

    # Encode and mess with the data (truncate)
    parts = msg.zmq_encode()
    parts[3] = parts[3][1:]

    with pytest.raises(ImgFormatError):
        dmsg = PubSubImageMessage.zmq_decode(parts)
        assert dmsg

    # Encode and mess with the data (append)
    parts = msg.zmq_encode()
    parts[3] = parts[3] + parts[3][2:]

    with pytest.raises(ImgFormatError):
        dmsg = PubSubImageMessage.zmq_decode(parts)
        assert dmsg


def test_multibyte_colors() -> None:
    """Test multibyte color sanity-checking"""
    msg = red_pixel_imagemessage()
    # 10 bit red
    msg.imginfo["bpp"] = 10
    msg.imgdata = bytes((0, 0, 0, 0, 3, 255))
    msg.sanity_check_imgdata()
    # 12 bit red
    msg.imginfo["bpp"] = 12
    msg.imgdata = bytes((0, 0, 0, 0, 15, 255))
    msg.sanity_check_imgdata()


def test_custom_sanitychecker() -> None:
    """Test the call to custom sanitychecker"""
    msg = MyFormatImageMsg("imgtopic")
    # Make one red pixel
    msg.imginfo["format"] = "myformat"
    msg.imginfo["w"] = 1
    msg.imginfo["h"] = 1
    # Test that the format specific one is used instead
    assert not msg.sanity_check_called
    msg.sanity_check_imgdata()
    assert msg.sanity_check_called


def test_compressed_warning(capsys):  # type: ignore
    """Check that warning is emitted if compressed format cannot be checked"""
    init_logging(logging.DEBUG)
    msg = red_pixel_imagemessage("imgtopic")
    msg.imginfo["format"] = "png"
    msg.imginfo["compressed"] = True
    warning_regex = r"\[WARNING\].*?Cannot sanity-check, (.*?) method not available"
    msg.sanity_check_imgdata()
    (_, stderr) = capsys.readouterr()
    matches = re.search(warning_regex, stderr, flags=re.MULTILINE)
    assert matches
    assert matches.group(1) == "sanity_check_png"
