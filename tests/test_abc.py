"""Test lower lever abstractions"""
from dataclasses import FrozenInstanceError

import pytest

from datastreamcorelib.abstract import RawMessage, ZMQSocketType, ZMQSocketDescription


def test_rawmsg_decode() -> None:
    """Tes rawmsg decodes correetly"""
    msgparts = [b"one", b"two", b"three"]
    msg = RawMessage.zmq_decode(msgparts)
    assert msg.raw_parts == msgparts


def test_rawmsg_roudtrip() -> None:
    """Tes rawmsg encoding decodes correetly"""
    msg = RawMessage()
    msg.raw_parts.append(b"foo")
    msg.raw_parts.append(b"vbar")
    msg.raw_parts.append(b"baz")
    msg2 = RawMessage.zmq_decode(msg.zmq_encode())
    assert msg2.raw_parts == msg.raw_parts


def test_socket_types() -> None:
    """Test that socket types can be treated as ints"""
    assert ZMQSocketType.PUB == 1  # type: ignore
    assert int(ZMQSocketType.PUB) + int(ZMQSocketType.SUB) == 3
    assert int(ZMQSocketType.REQ) + int(ZMQSocketType.REP) == 7


def test_socket_desc() -> None:
    """Check that ZMQSocketDescription post_init works as expected"""
    desc1 = ZMQSocketDescription("inproc://connectwraptest", ZMQSocketType.PUB)
    assert isinstance(desc1.socketuris, tuple)
    assert desc1.socketuris == (b"inproc://connectwraptest",)
    assert desc1.sockettype == ZMQSocketType.PUB
    with pytest.raises(FrozenInstanceError):
        desc1.socketuris = b"foo"  # type: ignore
