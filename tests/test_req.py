"""Test REQuest stuff"""
from typing import Union
import uuid
import logging

import pytest
from datastreamcorelib.datamessage import PubSubDataMessage
from datastreamcorelib.reqrep import REQMixinBase
from datastreamcorelib.abstract import (
    ZMQSocket,
    ZMQSocketDescription,
    ZMQSocketType,
    ZMQSocketUrisInputTypes,
)

from .test_rep import REPlyer
from .fixtures import FakeSocketHandler


LOGGER = logging.getLogger(__name__)


class REQuester(REQMixinBase):
    """Helper for testing REQuest stuff"""

    def __init__(self) -> None:
        self.replyer = REPlyer()
        self.sockethandler = FakeSocketHandler()

    def _get_request_socket(self, sockdef: Union[ZMQSocket, ZMQSocketUrisInputTypes]) -> ZMQSocket:
        """Get the socket"""
        if isinstance(sockdef, ZMQSocket):
            return sockdef
        sdesc = ZMQSocketDescription(sockdef, ZMQSocketType.REQ)
        return self.sockethandler.get_socket(sdesc)

    def _do_reqrep_blocking(
        self, sockdef: Union[ZMQSocket, ZMQSocketUrisInputTypes], msg: PubSubDataMessage
    ) -> PubSubDataMessage:
        """Do the actual REQuest and get the REPly (blocking context)"""
        _ = self._get_request_socket(sockdef)
        LOGGER.debug("passing {} to handle_rep_blocking".format(msg))
        return self.replyer.handle_rep_blocking(msg.zmq_encode())

    async def _do_reqrep_async(
        self, sockdef: Union[ZMQSocket, ZMQSocketUrisInputTypes], msg: PubSubDataMessage
    ) -> PubSubDataMessage:
        """Do the actual REQuest and get the REPly (async context)"""
        _ = self._get_request_socket(sockdef)
        LOGGER.debug("passing {} to handle_rep_async".format(msg))
        return await self.replyer.handle_rep_async(msg.zmq_encode())


def test_blocking_echo() -> None:
    """test the blocking echo"""
    random_str = str(uuid.uuid4())
    serv = REQuester()
    reply = serv.send_command_blocking(b"inproc://notused", "echo_blocking", "foo", "bar", random_str)

    assert reply.topic == b"reply"
    assert not reply.data["failed"]
    response = reply.data["response"]
    assert response[0] == "foo"
    assert response[-1] == random_str


def test_blocking_asyncecho() -> None:
    """test the blocking call to async echo"""
    random_str = str(uuid.uuid4())
    serv = REQuester()
    reply = serv.send_command_blocking(b"inproc://notused", "echo_async", "foo", "bar", random_str)
    assert reply.topic == b"reply"
    assert reply.data["failed"]
    assert reply.data["exception"]
    assert reply.data["reason"].startswith("RuntimeError")
    assert "is a coroutine and we are not capable of handling that" in reply.data["reason"]


@pytest.mark.asyncio
async def test_async_echo() -> None:
    """test the async echo"""
    random_str = str(uuid.uuid4())

    serv = REQuester()
    reply = await serv.send_command_async(b"inproc://notused", "echo_async", "foo", "bar", random_str)

    assert reply.topic == b"reply"
    assert not reply.data["failed"]
    response = reply.data["response"]
    assert response[0] == "foo"
    assert response[-1] == random_str


@pytest.mark.asyncio
async def test_async_true() -> None:
    """test the (blocking) true in async context"""
    serv = REQuester()
    reply = await serv.send_command_async(b"inproc://notused", "true")

    assert reply.topic == b"reply"
    assert not reply.data["failed"]
    assert reply.data["response"] is True


def test_blocking_fail_raise() -> None:
    """test the (blocking) fail in blocking context"""
    serv = REQuester()
    with pytest.raises(RuntimeError):
        serv.send_command_blocking(b"inproc://notused", "fail", raise_on_insane=True)


@pytest.mark.asyncio
async def test_async_fail_raise() -> None:
    """test the (blocking) fail in async context"""
    serv = REQuester()
    with pytest.raises(RuntimeError):
        await serv.send_command_async(b"inproc://notused", "fail", raise_on_insane=True)
