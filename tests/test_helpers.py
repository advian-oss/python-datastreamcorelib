"""Test helpers"""
import platform
from pathlib import Path


from datastreamcorelib.testhelpers import nice_tmpdir  # pylint: disable=W0611


# pylint: disable=W0621


def test_tmpdir(nice_tmpdir):  # type: ignore
    """Test the nice_tmpdir helper"""
    if platform.system() == "Darwin":
        assert str(nice_tmpdir).startswith("/tmp")  # nosec
    assert Path(nice_tmpdir).exists()
