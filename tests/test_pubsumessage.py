"""Test pubsubstuff."""
import pytest

from datastreamcorelib.pubsub import PubSubMessage
from datastreamcorelib.abstract import TooFewPartsError


def test_topic_bytes() -> None:
    """Make sure PubSubMessage topic gets converted to bytes."""
    msg = PubSubMessage("stringtopic1")
    assert msg.topic == b"stringtopic1"
    msg.topic = "stringtopic2"
    assert msg.topic == b"stringtopic2"  # type: ignore


def test_encode() -> None:
    """Test encoding to multipart list."""
    msg = PubSubMessage("stringtopic1")
    msg.dataparts = [b"baz", b"plonk"]
    assert msg.zmq_encode() == [b"stringtopic1", b"baz", b"plonk"]


def test_decode() -> None:
    """Test decoding from multipart list."""
    msg = PubSubMessage.zmq_decode([b"topic1", b"baz", b"plonk"])
    assert msg.topic == b"topic1"
    assert msg.dataparts == [b"baz", b"plonk"]


def test_encode_decode_roundtrip() -> None:
    """Make sure we can decode what we encode."""
    msg = PubSubMessage("stringtopic1")
    msg.dataparts = [str(item).encode("utf-8") for item in range(10)]
    enc = msg.zmq_encode()
    msg2 = PubSubMessage.zmq_decode(enc)
    assert msg2.topic == msg.topic
    assert msg2.dataparts == msg.dataparts


def test_decode_error_toofew() -> None:
    """Test decoding multipart message with too few parts"""
    with pytest.raises(TooFewPartsError):
        PubSubMessage.zmq_decode([b"datatopic"])
