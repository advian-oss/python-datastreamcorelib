"""Test hearbeat messages"""
from datastreamcorelib.datamessage import PubSubDataMessage
from datastreamcorelib.utils import create_heartbeat_message, MODRESOURCE_AVAILABLE


def test_create_heartbeat_message() -> None:
    """Test the creation helper"""
    msg = create_heartbeat_message()
    assert isinstance(msg, PubSubDataMessage)
    assert msg.topic == b"HEARTBEAT"
    assert "systemtime" in msg.data
    if MODRESOURCE_AVAILABLE:
        assert "resources" in msg.data
        assert "utime" in msg.data["resources"]
        assert msg.data["resources"]["utime"] > 0.0
