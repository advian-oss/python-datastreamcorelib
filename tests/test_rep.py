"""Test the REPly helpers"""
from typing import Any, Tuple
import asyncio
import time
import uuid

import pytest
from datastreamcorelib.datamessage import PubSubDataMessage
from datastreamcorelib.reqrep import REPMixinBase
from datastreamcorelib.binpackers import uuid_to_b64


class REPlyer(REPMixinBase):
    """For testing REPMixin"""

    async def echo_async(self, *args: Any) -> Any:
        """return the args"""
        _ = self
        await asyncio.sleep(0.01)
        return args

    def echo_blocking(self, *args: Any) -> Any:
        """return the args, blocking version"""
        _ = self
        time.sleep(0.01)
        return args

    def true(self) -> bool:
        """returns true, can be used to test invalid arg numbers etc"""
        _ = self
        return True

    def fail(self) -> None:
        """Raises error"""
        _ = self
        raise RuntimeError("you asked for failure")


def construct_command(cmd: str, *args: Any) -> Tuple[PubSubDataMessage, str]:
    """Helper to construct the command message"""
    msg = PubSubDataMessage(b"command")
    msgid_str = uuid_to_b64(msg.messageid)
    msg.data["command"] = [cmd] + list(args)
    return msg, msgid_str


def test_blocking_echo() -> None:
    """test the blocking echo"""
    random_str = str(uuid.uuid4())
    msg, msgid_str = construct_command("echo_blocking", "foo", "bar", random_str)

    serv = REPlyer()
    reply = serv.handle_rep_blocking(msg.zmq_encode())

    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert not reply.data["failed"]
    response = reply.data["response"]
    assert response[0] == "foo"
    assert response[-1] == random_str


def test_blocking_asyncecho() -> None:
    """test the blocking call to async echo"""
    random_str = str(uuid.uuid4())
    msg, msgid_str = construct_command("echo_async", "foo", "bar", random_str)

    serv = REPlyer()
    reply = serv.handle_rep_blocking(msg.zmq_encode())
    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert reply.data["failed"]
    assert reply.data["exception"]
    assert reply.data["reason"].startswith("RuntimeError")
    assert "is a coroutine and we are not capable of handling that" in reply.data["reason"]


@pytest.mark.asyncio
async def test_async_echo() -> None:
    """test the async echo"""
    random_str = str(uuid.uuid4())
    msg, msgid_str = construct_command("echo_async", "foo", "bar", random_str)

    serv = REPlyer()
    reply = await serv.handle_rep_async(msg.zmq_encode())

    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert not reply.data["failed"]
    response = reply.data["response"]
    assert response[0] == "foo"
    assert response[-1] == random_str


@pytest.mark.asyncio
async def test_async_blockingecho() -> None:
    """test the blocking echo in async context"""
    random_str = str(uuid.uuid4())
    msg, msgid_str = construct_command("echo_blocking", "foo", "bar", random_str)

    serv = REPlyer()
    reply = await serv.handle_rep_async(msg.zmq_encode())

    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert not reply.data["failed"]
    response = reply.data["response"]
    assert response[0] == "foo"
    assert response[-1] == random_str


@pytest.mark.asyncio
async def test_async_true() -> None:
    """test the (blocking) true in async context"""
    msg, msgid_str = construct_command("true")

    serv = REPlyer()
    reply = await serv.handle_rep_async(msg.zmq_encode())

    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert not reply.data["failed"]
    assert reply.data["response"] is True


def test_blocking_true() -> None:
    """test the (blocking) true in blocking context"""
    msg, msgid_str = construct_command("true")

    serv = REPlyer()
    reply = serv.handle_rep_blocking(msg.zmq_encode())

    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert not reply.data["failed"]
    assert reply.data["response"] is True


def test_blocking_fail() -> None:
    """test the (blocking) fail in blocking context"""
    msg, msgid_str = construct_command("fail")

    serv = REPlyer()
    reply = serv.handle_rep_blocking(msg.zmq_encode())
    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert reply.data["failed"]
    assert reply.data["exception"]
    assert reply.data["reason"].startswith("RuntimeError")
    assert "you asked for failure" in reply.data["reason"]


@pytest.mark.asyncio
async def test_async_fail() -> None:
    """test the (blocking) fail in async context"""
    msg, msgid_str = construct_command("fail")

    serv = REPlyer()
    reply = await serv.handle_rep_async(msg.zmq_encode())
    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert reply.data["failed"]
    assert reply.data["exception"]
    assert reply.data["reason"].startswith("RuntimeError")
    assert "you asked for failure" in reply.data["reason"]


def test_blocking_invalidargs() -> None:
    """invalid args in blocking context"""
    msg, msgid_str = construct_command("true", "unexpected arg")

    serv = REPlyer()
    reply = serv.handle_rep_blocking(msg.zmq_encode())
    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert reply.data["failed"]
    assert reply.data["exception"]
    assert reply.data["reason"].startswith("TypeError")


@pytest.mark.asyncio
async def test_async_invalidargs() -> None:
    """invalid args in async context"""
    msg, msgid_str = construct_command("true", "unexpected arg")

    serv = REPlyer()
    reply = await serv.handle_rep_async(msg.zmq_encode())
    assert reply.topic == b"reply"
    assert reply.data["cmd_msgid"] == msgid_str
    assert reply.data["failed"]
    assert reply.data["exception"]
    assert reply.data["reason"].startswith("TypeError")
