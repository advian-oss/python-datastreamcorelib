"""Test the async variants"""
from typing import List

import pytest

from datastreamcorelib.abstract import ZMQSocketType, ZMQSocketDescription
from datastreamcorelib.pubsub import Subscription, PubSubMessage
from .fixtures import FakeSocketHandler, FakePubSubManager, CBTestException


@pytest.mark.asyncio
async def test_async_dispatch_async_cb() -> None:
    """Test the async dispatch method"""
    received_msgs: List[PubSubMessage] = []

    async def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    sub = Subscription("fake", "asyncd_asynccb", success_callback, isasync=True)
    msg = PubSubMessage("asyncd_asynccb", [b"foo"])
    await sub.zmq_decode_and_dispatch_async(msg.zmq_encode())
    assert received_msgs


@pytest.mark.asyncio
async def test_async_dispatch_sync_cb() -> None:
    """Test the async dispatch method"""
    received_msgs: List[PubSubMessage] = []

    def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    sub = Subscription("fake", "asyncd_synccb", success_callback, isasync=False)
    msg = PubSubMessage("asyncd_synccb", [b"foo"])
    await sub.zmq_decode_and_dispatch_async(msg.zmq_encode())
    # TODO: capture log and  check for the warning message
    # TODO: capture log and  check for the error message
    # It still dispatches because we are cool
    assert received_msgs


@pytest.mark.asyncio
async def test_sync_dispatch_sync_cb() -> None:
    """Test the async dispatch method"""
    received_msgs: List[PubSubMessage] = []

    def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    sub = Subscription("fake", "syncd_synccb", success_callback, isasync=False)
    msg = PubSubMessage("syncd_synccb", [b"foo"])
    sub.zmq_decode_and_dispatch(msg.zmq_encode())
    assert received_msgs


@pytest.mark.asyncio
async def test_psmgr_async_dispatch_async_cb() -> None:
    """Test the async dispatch method with async callback"""
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr1", b"addr2"], ZMQSocketType.SUB)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)
    received_msgs: List[PubSubMessage] = []

    async def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    sub = Subscription(sockdesc.socketuris, "async_cb", success_callback, isasync=True)
    sminstance.subscribe(sub)
    assert sub in sminstance.subscriptions

    msg = PubSubMessage("async_cb", [b"foo"])
    await sminstance.raw_zmq_callback_async(sockinst, msg.zmq_encode())
    assert received_msgs


@pytest.mark.asyncio
async def test_psmgr_async_dispatch_async_cb_exception() -> None:
    """Test the async dispatch method with async callback"""
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr3", b"addr4"], ZMQSocketType.SUB)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)

    async def success_callback_exc(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        """raise error"""
        (_, _) = (sub, msg)
        raise CBTestException("Testing exceptions")

    sub = Subscription(sockdesc.socketuris, "exceptiontest", success_callback_exc, isasync=True)
    sminstance.subscribe(sub)
    assert sub in sminstance.subscriptions

    msg = PubSubMessage("exceptiontest", [b"foo"])
    with pytest.raises(CBTestException):
        await sminstance.raw_zmq_callback_async(sockinst, msg.zmq_encode())


@pytest.mark.asyncio
async def test_psmgr_async_dispatch_sync_cb() -> None:
    """Test the async dispatch method with sync callback"""
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr5", b"addr6"], ZMQSocketType.SUB)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)
    received_msgs: List[PubSubMessage] = []

    def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    sub = Subscription(sockdesc.socketuris, "sync_cb", success_callback, isasync=False)
    sminstance.subscribe(sub)
    assert sub in sminstance.subscriptions

    msg = PubSubMessage("sync_cb", [b"foo"])
    # TODO: capture log and  check for the warning message
    # TODO: capture log and  check for the error message
    await sminstance.raw_zmq_callback_async(sockinst, msg.zmq_encode())
    assert received_msgs


@pytest.mark.asyncio
async def test_psmgr_failure_callback_async() -> None:
    """Test failure callbacks"""
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr7", b"addr8"], ZMQSocketType.SUB)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)
    failure_cb_called = False

    async def failure_callback(sub: Subscription, msgparts: List[bytes], exc: Exception) -> None:
        """handle error"""
        nonlocal failure_cb_called
        (_, _) = (sub, msgparts)
        assert isinstance(exc, CBTestException)
        failure_cb_called = True

    async def success_callback_exc(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        """raise error"""
        (_, _) = (sub, msg)
        raise CBTestException("Testing exceptions")

    sub = Subscription(
        sockdesc.socketuris, "failcbtest", success_callback_exc, isasync=True, failure_callback=failure_callback
    )
    sminstance.subscribe(sub)
    assert sub in sminstance.subscriptions

    msg = PubSubMessage("failcbtest", [b"foo"])
    with pytest.raises(CBTestException):
        await sminstance.raw_zmq_callback_async(sockinst, msg.zmq_encode())

    assert failure_cb_called


@pytest.mark.asyncio
async def test_psmgr_failure_callback_sync() -> None:
    """Test failure callbacks"""
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr9", b"addr10"], ZMQSocketType.SUB)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)
    failure_cb_called = False

    def failure_callback_sync(sub: Subscription, msgparts: List[bytes], exc: Exception) -> None:
        """handle error"""
        nonlocal failure_cb_called
        (_, _) = (sub, msgparts)
        assert isinstance(exc, CBTestException)
        failure_cb_called = True

    def success_callback_exc_sync(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        """raise error"""
        (_, _) = (sub, msg)
        raise CBTestException("Testing exceptions")

    sub = Subscription(
        sockdesc.socketuris,
        "failcbtest_sync",
        success_callback_exc_sync,
        isasync=False,
        failure_callback=failure_callback_sync,
    )
    sminstance.subscribe(sub)
    assert sub in sminstance.subscriptions

    msg = PubSubMessage("failcbtest_sync", [b"foo"])
    with pytest.raises(CBTestException):
        sminstance.raw_zmq_callback(sockinst, msg.zmq_encode())

    assert failure_cb_called


@pytest.mark.asyncio
async def test_psmgr_async_dispatch_topicmatch() -> None:
    """Make sure topics are matched only by prefix"""
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr11", b"addr12"], ZMQSocketType.SUB)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)
    received_msgs: List[PubSubMessage] = []

    async def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    sub = Subscription(sockdesc.socketuris, "baz", success_callback, isasync=True)
    sminstance.subscribe(sub)
    assert sub in sminstance.subscriptions

    msg = PubSubMessage("foo_bar_baz", [b"foo"])
    await sminstance.raw_zmq_callback_async(sockinst, msg.zmq_encode())
    assert not received_msgs

    msg = PubSubMessage("baz_foo", [b"foo"])
    await sminstance.raw_zmq_callback_async(sockinst, msg.zmq_encode())
    assert received_msgs


@pytest.mark.asyncio
async def test_psmgr_sync_dispatch_topicmatch() -> None:
    """Make sure topics are matched only by prefix (sync dispatch)"""
    sminstance = FakePubSubManager.instance(FakeSocketHandler)
    sockdesc = ZMQSocketDescription([b"addr13", b"addr14"], ZMQSocketType.SUB)
    sockinst = sminstance.sockethandler.get_socket(sockdesc)
    received_msgs: List[PubSubMessage] = []

    def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    sub = Subscription(sockdesc.socketuris, "baz", success_callback, isasync=False)
    sminstance.subscribe(sub)
    assert sub in sminstance.subscriptions

    msg = PubSubMessage("foo_bar_baz", [b"foo"])
    sminstance.raw_zmq_callback(sockinst, msg.zmq_encode())
    assert not received_msgs

    msg = PubSubMessage("baz_foo", [b"foo"])
    sminstance.raw_zmq_callback(sockinst, msg.zmq_encode())
    assert received_msgs
