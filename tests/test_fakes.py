"""Test the fake wrappers"""
from typing import cast

from datastreamcorelib.abstract import ZMQSocketType, ZMQSocketDescription, BaseSocketHandler, SOCKETHANDLER_SINGLETONS
from .fixtures import FakeSocket, FakeSocketHandler


def test_fake_subscription_topic_added() -> None:
    """Test the raw fake socket subscription"""
    fakesub = FakeSocket(ZMQSocketType.SUB)
    fakesub.subscribe(b"Foobar")
    assert b"Foobar" in fakesub.topics


def test_sockethandler_singleton() -> None:
    """Test the singleton method and inheritance"""
    handler = FakeSocketHandler.instance()
    old_len = len(SOCKETHANDLER_SINGLETONS)
    assert old_len
    assert isinstance(handler, FakeSocketHandler)
    assert isinstance(handler, BaseSocketHandler)
    handler2 = FakeSocketHandler.instance()
    assert handler == handler2
    assert len(SOCKETHANDLER_SINGLETONS) == old_len


def test_fake_handler_subscription() -> None:
    """Test subscription via the socket handler"""
    socketdesc1 = ZMQSocketDescription([b"addr1", b"addr2"], ZMQSocketType.SUB)
    handler = FakeSocketHandler.instance()
    assert isinstance(handler, FakeSocketHandler)
    socket1 = cast(FakeSocket, handler.get_socket(socketdesc1))
    socket1.subscribe(b"Foobar")
    assert b"Foobar" in socket1.topics


def test_socket_close() -> None:
    """Test the close method and getting a fresh socket"""
    socketdesc1 = ZMQSocketDescription([b"addr1", b"addr2"], ZMQSocketType.SUB)
    handler = FakeSocketHandler.instance()
    socket1 = cast(FakeSocket, handler.get_socket(socketdesc1))
    assert not socket1.closed
    assert socket1 in handler.open_sockets
    socket1.close()
    assert socket1.closed
    assert socket1 not in handler.open_sockets
    socket2 = cast(FakeSocket, handler.get_socket(socketdesc1))
    assert not socket2.closed
    assert socket2.__hash__() != socket1.__hash__()  # pylint: disable=C2801


def test_close_all_sockets() -> None:
    """Test the close_all_sockets method"""
    socketdesc1 = ZMQSocketDescription([b"addr1", b"addr2"], ZMQSocketType.SUB)
    handler = FakeSocketHandler.instance()
    socket1 = cast(FakeSocket, handler.get_socket(socketdesc1))
    assert not socket1.closed
    socket1.subscribe(b"Foobar")
    assert b"Foobar" in socket1.topics
    handler.close_all_sockets()
    assert socket1.closed
    assert socket1 not in handler.open_sockets
    # Get make sure we get a fresh one.
    socket2 = cast(FakeSocket, handler.get_socket(socketdesc1))
    assert not socket2.topics
    assert not socket2.closed
