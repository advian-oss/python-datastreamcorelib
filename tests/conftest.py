"""pytest fixtures we want everywhere"""
import logging

import pytest

from libadvian.logging import init_logging


@pytest.fixture(autouse=True)
def debug_log_initialized() -> None:
    """init logging to debug for every test"""
    init_logging(logging.DEBUG)
